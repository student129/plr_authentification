﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HCI;
using UnityEngine.Video;

using UnityEngine.SceneManagement;
public class Script_scene1_video : MonoBehaviour
{


    private new GameObject camera;

    private VideoPlayer videoPlayer;
 
    
    public GameObject gazeMarkerObject;
    public Camera cameracolor;

   
    public float timer = 0.0f;
    //private string url;

    private string url  = "file://Assets/Resources/Wars.mp4";
  //  public int count = 0;

    public int sceneIndex = 2;

  //  public Timer counter;
  //  private double elapsedTime = 0;
    public bool Isvideo { get; set; } = false;
    // public Boolean Isvideo { get; set; } = false;
    private Color currentcolor= Color.gray;





    void startcolor(Color color)
    {
        camera = GameObject.Find("LeftEye");
        cameracolor = camera.AddComponent<Camera>();
       // cameracolor = GetComponent<Camera>();
        cameracolor.clearFlags = CameraClearFlags.SolidColor;
        cameracolor.backgroundColor = color;
    }
    void startvideo(string CameraName)
    {
        camera = GameObject.Find(CameraName);
        videoPlayer = camera.AddComponent<VideoPlayer>();
        videoPlayer.playOnAwake = false;
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;
        videoPlayer.targetCameraAlpha = 0.5F;
        videoPlayer.url = Application.persistentDataPath+"/Wars.MP4";
       // videoPlayer.url = "file://Assets/Resources/Wars.mp4";
        videoPlayer.frame = 100;
        videoPlayer.isLooping = true;
        videoPlayer.Play();
    }
    // Start is called before the first frame update
    void Start()
    {

        if (Isvideo) { 
            startvideo("LeftEye"); 
            startvideo("RightEye");
           // StartCoroutine(ExampleFinishAfter10Seconds());
        } else {
            startcolor(currentcolor);
         
        }

         int participantID = GameObject.Find("StudyManager").GetComponent<StudyManager>().GetParticipant()-95;

        // enable gaze logging
          GameObject.Find("GazeManager").GetComponent<GazeLogger>().StartLogging("Study1Scenevideo_", participantID);

        // activate object following gaze
         GameObject.Find("GazeManager").GetComponent<GazeRaycast>().StartGazeRaycasting(gazeMarkerObject);
    }


    // Update is called once per frame


    void Update()
    {

        timer += Time.deltaTime;

        if (timer >= 10 && !Isvideo && currentcolor != Color.black)
        {
            currentcolor = Color.black;
            timer = 0;
            print("scene 1 change color");
            Start();

        }else if (timer >= 10 && currentcolor == Color.black)
        {
            Isvideo = true;
            timer = 0;
            Start();
        }else if (timer >= 10 && Isvideo)
        {
            videoPlayer.Stop();
            Destroy(GetComponent<VideoPlayer>());
            print("scene 1 FIND");
        }

       
    }

    private void OnDestroy()  // Called when the scene ends by being unloaded in ExampleFinishAfter5Seconds()
    {
        // disable logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StopLogging();

        // deactivate object following gaze
        GameObject.Find("GazeManager").GetComponent<GazeRaycast>().gazeMarkerObj = null;

        // set atom to be finished
        GameObject.Find("StudyManager").GetComponent<StudyManager>().GetCurrentStudyAtom().SetHasFinished();


        videoPlayer.Stop();
       // Destroy(GetComponent<VideoPlayer>());

    }
   /* private void OnTimedEvent(System.Object source, System.Timers.ElapsedEventArgs e)
    {
        print("The Elapsed event was raised at {0}");

        count = count + 1;
    }*/

    IEnumerator ExampleFinishAfter10Seconds()
    {
        yield return new WaitForSeconds(10);

        // finish scene after five seconds
        SceneManager.UnloadSceneAsync(4);
    }
}

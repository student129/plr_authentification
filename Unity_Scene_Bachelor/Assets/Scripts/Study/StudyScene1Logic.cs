﻿using System;
using System.Collections;
using System.Collections.Generic;
using HCI;
using UnityEditor;
using UnityEngine;

using UnityEngine.SceneManagement;

/*
 * Important: this script is part of an object that is loaded additive to the `LaunchScene`.
 * We interact with the LaunchScene's objects via GameObject.Find("someObjectName").
 * This call is rather costly so try to not use it in the Update()-method but create references instead.
 * OnDestroy() is called when the Scene unloads.
 */

public class StudyScene1Logic : MonoBehaviour
{
    public int sceneIndex = 1; // This must be the number that is set in File > Build Settings for this scene
    public GameObject gazeMarkerObject;
    public GameObject Wall ;
    public bool Isgrau { get; set; } = true;

    private Camera cam;
    public float timer = 0.0f;
    void StartCamera(string CameraName, Color color)
    {
        cam = GameObject.Find(CameraName).GetComponent<Camera>();
        cam.clearFlags = CameraClearFlags.SolidColor;
        cam.backgroundColor = color;

    }

    // Start is called before the first frame update
    void Start()
    {
        if (Isgrau) {
            gazeMarkerObject.SetActive(false);
            Wall.SetActive(false);
            StartCamera("LeftEye", Color.gray);
            StartCamera("RightEye", Color.gray);
        }
      //  Lightmapping.ClearLightingDataAsset();
        int participantID = GameObject.Find("StudyManager").GetComponent<StudyManager>().GetParticipant() -95;
        
        // enable gaze logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StartLogging("Study1Scene_", participantID);
        /*
                GameObject.Find("GazeManager").GetComponent<GazeRaycast>().StartGazeRaycasting(gazeMarkerObject);
                // end scene after some seconds
                StartCoroutine(ExampleFinishAfter10Seconds());*/
        Wall.transform.parent = cam.transform;
    }

    IEnumerator ExampleFinishAfter60Seconds()
    {
        yield return new WaitForSeconds(60);//60

        // finish scene after five seconds
        SceneManager.UnloadSceneAsync(4);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >=60 && Isgrau)//60
        {
            GameObject.Find("GazeManager").GetComponent<GazeLogger>().writeMarker();
            StartCamera("LeftEye", Color.black);
            StartCamera("RightEye", Color.black);
            gazeMarkerObject.SetActive(true);
            Wall.SetActive(true);
           
            Isgrau = false;
            // activate object following gaze
            GameObject.Find("GazeManager").GetComponent<GazeRaycast>().StartGazeRaycasting(gazeMarkerObject);
            // end scene after some seconds
            StartCoroutine(ExampleFinishAfter60Seconds());
            timer = 0;
           
        }
        // TODO: Implement your scene functionality here.
        
    }

    private void OnDestroy()  // Called when the scene ends by being unloaded in ExampleFinishAfter5Seconds()
    {
        gazeMarkerObject.SetActive(false);
        Wall.SetActive(false);
        // disable logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StopLogging();
        
        // deactivate object following gaze
        GameObject.Find("GazeManager").GetComponent<GazeRaycast>().gazeMarkerObj = null;
        
        // set atom to be finished
        GameObject.Find("StudyManager").GetComponent<StudyManager>().GetCurrentStudyAtom().SetHasFinished();
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

namespace HCI
{
    public class StudyManager : MonoBehaviour
    {
        [Header("General")]
        public GameObject camera;
        public GameObject input;
        public GameObject logger;

        [Header("Dialog")]     
        public GameObject dialogCanvas;
        public GameObject dialogText;
        public GameObject dialogSlider;
        public GameObject dialogButton;

        private List<Atom> _studyAtoms;
        private int nextscene = 1;
        private Atom _currentStudyAtom;

        [HideInInspector]
        public int participant = -1;

        [HideInInspector]
        public int[][] sequences =   // Example for some latin square randomization
                {
                  // new int[] {4, 5 ,3, 1, 2},
                   new int[] {1, 4 ,5, 3, 2},
                   new int[] {5, 2, 4, 1, 3},
                   new int[] {2,3 , 1, 5, 4},
                   new int[] {4, 1, 3, 2, 5},
                   new int[] {3, 5, 2, 4, 1},

                   new int[] {1, 4 ,5, 3, 2},
                   new int[] {5, 2, 4, 1, 3},
                   new int[] {2,3 , 1, 5, 4},
                   new int[] {4, 1, 3, 2, 5},
                   new int[] {3, 5, 2, 4, 1},

                   new int[] {1, 4 ,5, 3, 2},
                   new int[] {5, 2, 4, 1, 3},
                   new int[] {2,3 , 1, 5, 4},
                   new int[] {4, 1, 3, 2, 5},
                   new int[] {3, 5, 2, 4, 1},

                   new int[] {1, 4 ,5, 3, 2},
                   new int[] {5, 2, 4, 1, 3},
                   new int[] {2,3 , 1, 5, 4},
                   new int[] {4, 1, 3, 2, 5},
                   new int[] {3, 5, 2, 4, 1},

                };

        
        void Start()
        {
            // initialize study start atoms
            this.InitStudyStartAtoms();

            // no study atom selected in the beginning
            this._currentStudyAtom = null;
        }

        void Update()
        {
            // check for user keyboard input for debugging
            if (Input.GetKeyDown(KeyCode.Space))
                this.OnButtonPressed();

            // if no study atom is selected, then get one from the list
            if (this._currentStudyAtom == null)
                this._currentStudyAtom = InitNextStudyAtom();

            // if no study atom is selected, then the study is finished
            if (this._currentStudyAtom == null)
                return;

            // run current study atom
            this._currentStudyAtom.Update();

            // if study atom has finished, then we remove it
            if (this._currentStudyAtom.GetHasFinished())
                this._currentStudyAtom = null;
        }

        private void InitStudyStartAtoms()
        {
            // create new list with study atoms
            this._studyAtoms = new List<Atom>();

            this._studyAtoms.Add(new AtomDialogOkay(this, "Welcome to our study!\n" +
                                                         "Please click 'Okay' to continue."));
            // show info. Legal info is mandatory.
            this._studyAtoms.Add(new AtomDialogOkay(this, "Attention: this application " +
                                                          "records and stores your eye gaze data as " +
                                                          "described in the corresponding consent form. Thank " +
                                                          " you for participating in this experiment."));
            this._studyAtoms.Add(new AtomDialogRange(this, "Please select the correct " +
                                                          "participant ID and confirm with clicking on 'Okay'."));
        }

        private Atom InitNextStudyAtom()
        {
            // check if another study atom exists
            if (this._studyAtoms.Count <= 0)
                return null;

            // get next study atom
            Atom selected = this._studyAtoms[0];

            // start next study atom
            selected.Start();

            // remove the study atom from the list
            this._studyAtoms.RemoveAt(0);

            // return the selected study atom
            return selected;
        }

        public void SetParticipant(int participantID)
        {
            this.participant = participantID;


            Debug.Log("participantid : " + this.participant);
            // REQUIREMENT: Dialog range has been called an a valid participant id is set
            // get sequence for participant
            var sequence = this.sequences[this.participant - 1];

             nextscene = sequence[1];
            // create all atoms for sequence
            for (int i = 0; i < sequence.Length; i++)
            {

                if (i==0)
                {
                    this._studyAtoms.Add(new AtomDialogDelay(this, " please put the controller behind you . " + sequence[i]));
                }
              
                // switch scene
               
                if (sequence[i]== 4) {
                    // show info
                    this._studyAtoms.Add(new AtomDialogDelay(this, " please move your eyes. " + sequence[i]));
                    // this._studyAtoms.Add(new AtomDialogDelay(this, "Please inform the experimenter about <TODO>!", 10));
                    //this._studyAtoms.Add(new AtomDialogOkay(this, "Click 'okay' as soon as you answered\nthe questions of the experimenter."));
                }
                this._studyAtoms.Add(new AtomScene(this, sequence[i]));
            }

            this._studyAtoms.Add(new AtomDialogOkay(this, "Please take off the headset now."));
        }

        public void OnSliderChange()
        {
            Debug.Log("OnSlideChange invoked.");

            // forward event to study atom
            if (this._currentStudyAtom != null)
                this._currentStudyAtom.OnSliderChange();
        }

        public void OnButtonPressed()
        {
            Debug.Log("OnButtonChange invoked.");

            // forward event to study atom
            if (this._currentStudyAtom != null)
                this._currentStudyAtom.OnButtonPressed();
        }

        public ref Atom GetCurrentStudyAtom()
        {
            return ref this._currentStudyAtom;
        }

        public int GetParticipant()
        {
            Debug.Log("participantid : " + this.participant);
            return this.participant;
        }
        public int GetNextscene()
        {
            Debug.Log("Nextscene : " + this.nextscene);
            return this.nextscene;
        }
    }
}

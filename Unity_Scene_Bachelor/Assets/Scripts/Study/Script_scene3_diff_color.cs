﻿using System.Collections;
using UnityEngine;
using HCI;
using UnityEngine.SceneManagement;
public class Script_scene3_diff_color : MonoBehaviour
{

    public Camera cameraLeft;
    public Camera cameraRight;
    public int IsoneEye = 0;
    //{ get; set; } 
    public int sceneIndex = 2;
    
   
    private double elapsedTime = 0;



    public float timer = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        cameraRight = GameObject.Find("LeftEye").GetComponent<Camera>();
        cameraLeft = GameObject.Find("RightEye").GetComponent<Camera>();
       
       

       

        if (IsoneEye == 0)
        {
            cameraLeft.backgroundColor = Color.gray;
            cameraRight.backgroundColor = Color.gray;
        }
        else if (IsoneEye == 1)
        {
            cameraLeft.backgroundColor = Color.white;
            cameraRight.backgroundColor = Color.white;
        }
        else if (IsoneEye == 2)
        {
            cameraLeft.backgroundColor = Color.white;
            cameraRight.backgroundColor = Color.black;
            StartCoroutine(ExampleFinishAfter10Seconds(60));

        }
        
        int participantID = GameObject.Find("StudyManager").GetComponent<StudyManager>().GetParticipant()-95;

        // enable gaze logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StartLogging("StudyScene3diff_", participantID);

      
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 60 && IsoneEye == 0)
        {
           // print("scene 3 is over");
            IsoneEye = 1;

            timer = 0;
            Start();
        }
        if (timer >= 60 && IsoneEye ==1 )
        {
              //print("scene 3 is over");
            IsoneEye = 2;
            timer = 0;
            Start();
        }


    }


   

    private void OnDestroy()  // Called when the scene ends by being unloaded in ExampleFinishAfter5Seconds()
    {
        // disable logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StopLogging();

        cameraLeft.backgroundColor = Color.black;
        // set atom to be finished
        GameObject.Find("StudyManager").GetComponent<StudyManager>().GetCurrentStudyAtom().SetHasFinished();
    }

    IEnumerator ExampleFinishAfter10Seconds(int time)
    {
        yield return new WaitForSeconds(time);

        // finish scene after five seconds
        SceneManager.UnloadSceneAsync(3);
    }
}

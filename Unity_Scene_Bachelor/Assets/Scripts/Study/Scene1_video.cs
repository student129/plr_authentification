﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HCI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
public class Scene1_video : MonoBehaviour
{
    public int sceneIndex = 1; // This must be the number that is set in File > Build Settings for this scene

    private VideoPlayer videoPlayer;

    private new GameObject camera;
    private Camera cam;
    public float timer = 0.0f;
    public bool Isvideo { get; set; } = false;
    public bool Isgrau { get; set; } =true;
    void StartCamera(string CameraName, Color color)
    {
        cam = GameObject.Find(CameraName).GetComponent<Camera>();
        cam.clearFlags = CameraClearFlags.SolidColor;
        cam.backgroundColor = color;

    }
    void removeplayer(string CameraName) {
        camera = GameObject.Find(CameraName);
        videoPlayer = camera.GetComponent<VideoPlayer>();
        Destroy(videoPlayer);
    }
    void startvideo(string CameraName)
    {
        camera = GameObject.Find(CameraName);
        videoPlayer = camera.AddComponent<VideoPlayer>();
        videoPlayer.playOnAwake = true;
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;
        videoPlayer.targetCameraAlpha = 0.5F;
        videoPlayer.url = Application.persistentDataPath + "/Wars.MP4";
        //videoPlayer.url = "file://Assets/Resources/Wars.mp4";
        videoPlayer.frame = 100;
        videoPlayer.isLooping = true;
        videoPlayer.Play();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (Isvideo) {
            startvideo("LeftEye");
            startvideo("RightEye");
        }
        else
        {
            StartCamera("LeftEye",Color.gray);
            StartCamera("RightEye", Color.gray);
        }
        //  Lightmapping.ClearLightingDataAsset();
        int participantID = GameObject.Find("StudyManager").GetComponent<StudyManager>().GetParticipant() - 95;

        // enable gaze logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StartLogging("StudyScene1_", participantID);

        // end scene after some seconds
      //  StartCoroutine(ExampleFinishAfter10Seconds());
    }

    IEnumerator ExampleFinishAfter60Seconds()
    {
        yield return new WaitForSeconds(60);//60

        // finish scene after five seconds
        SceneManager.UnloadSceneAsync(1);
    }




    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;

        if (timer >= 60 && Isgrau)//60
        {

            // TODO: Implement your scene functionality here.
            GameObject.Find("GazeManager").GetComponent<GazeLogger>().writeMarker();
            Debug.Log("Update() in StudyScene1Logic.cs");
            StartCamera("LeftEye", Color.black);
            StartCamera("RightEye", Color.black);
            startvideo("LeftEye");
            startvideo("RightEye");
            timer = 0;
             Isvideo = true;
            Isgrau = false;

        }
        if (timer >= 150 && Isvideo && !Isgrau) //150
        {
            // TODO: Implement your scene functionality here.
            Debug.Log("Update() in StudyScene1Logic.cs");
            removeplayer("LeftEye");
            removeplayer("RightEye");
            timer = 0;
            StartCoroutine(ExampleFinishAfter60Seconds());


        }
           
    }

    private void OnDestroy()  // Called when the scene ends by being unloaded in ExampleFinishAfter5Seconds()
    {
       
        // disable logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StopLogging();

        // set atom to be finished
        GameObject.Find("StudyManager").GetComponent<StudyManager>().GetCurrentStudyAtom().SetHasFinished();


    }
}

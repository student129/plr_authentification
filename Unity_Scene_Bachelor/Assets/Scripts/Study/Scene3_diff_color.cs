﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HCI;
using UnityEngine.SceneManagement;
public class Scene3_diff_color : MonoBehaviour
{
    private new GameObject camera;
    private Camera cam;
    public float timer = 0.0f;
    public bool Isgrau { get; set; } = true;
    void StartCamera(string CameraName, Color color)
    {
        cam = GameObject.Find(CameraName).GetComponent<Camera>();
        cam.clearFlags = CameraClearFlags.SolidColor;
        cam.backgroundColor = color;

    }
    // Start is called before the first frame update
    void Start()
    {

        StartCamera("LeftEye", Color.gray);
        StartCamera("RightEye", Color.gray);

        //  Lightmapping.ClearLightingDataAsset();
        int participantID = GameObject.Find("StudyManager").GetComponent<StudyManager>().GetParticipant() - 95;

        // enable gaze logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StartLogging("StudyScene3_", participantID);

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= 60 && Isgrau)
        {
            

            GameObject.Find("GazeManager").GetComponent<GazeLogger>().writeMarker();
            StartCamera("LeftEye", Color.black);
            StartCamera("RightEye", Color.black);
            timer = 0;
            Isgrau = false;
        }

        if (timer >= 60 && !Isgrau)
        {
            StartCamera("LeftEye", Color.black);
            StartCamera("RightEye", Color.white);
            StartCoroutine(ExampleFinishAfter60Seconds());
        }
    }



    private void OnDestroy()  // Called when the scene ends by being unloaded in ExampleFinishAfter5Seconds()
    {

        // disable logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StopLogging();

        // set atom to be finished
        GameObject.Find("StudyManager").GetComponent<StudyManager>().GetCurrentStudyAtom().SetHasFinished();

        StartCamera("RightEye", Color.black);
    }



    IEnumerator ExampleFinishAfter60Seconds()
    {
        yield return new WaitForSeconds(60);

        // finish scene after five seconds
        SceneManager.UnloadSceneAsync(3);
    }

}
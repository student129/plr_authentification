﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using HCI;
using UnityEngine.SceneManagement;
public class Script_scene2_color : MonoBehaviour
{

    //public Camera camera;
    public Camera camera;
    public float timer = 0.0f;
    public int Iscolor { get; set; } = 0;

    

    private double elapsedTime = 0;
  
    public GameObject gazeMarkerObject;
   

    void start_grau()
    {
        Camera cam = gameObject.GetComponent<Camera>();
        camera = GetComponent<Camera>();
        camera.clearFlags = CameraClearFlags.SolidColor;
        camera.backgroundColor = Color.gray;
      
    }

    void start_black()
    {
        camera = GetComponent<Camera>();
        camera.clearFlags = CameraClearFlags.SolidColor;
        camera.backgroundColor = Color.black;
       
    }


    void start_white()
    {
      
        camera = GetComponent<Camera>();
        camera.clearFlags = CameraClearFlags.SolidColor;
        camera.backgroundColor = Color.white;
       

    }

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(ExampleFinishAfter10Seconds(6));

        if (Iscolor == 0)
        {
            start_grau();
           
        }
        else if (Iscolor == 1)
        {
            start_black();
           
        }
        else if (Iscolor == 2)
        {
            start_white();
          

        }

        int participantID = GameObject.Find("StudyManager").GetComponent<StudyManager>().GetParticipant() - 95;

        // enable gaze logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StartLogging("StudyScene2_", participantID);

        // activate object following gaze
        GameObject.Find("GazeManager").GetComponent<GazeRaycast>().StartGazeRaycasting(gazeMarkerObject);

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
       
        if (timer >= 10 && camera.backgroundColor == Color.gray)
        {
            Iscolor = 1;
            timer = 0;
             Start();
        }

        else if (timer >= 10 && camera.backgroundColor == Color.black)
        {//1 minut
            Iscolor = 2;
            timer = 0;
            Start();
            
        }
        else if (timer >= 5 && camera.backgroundColor == Color.white)
        {
            StartCoroutine(ExampleFinishAfter10Seconds(1));
            print("scene 2 is over");
            timer = 0;


        }
        



    }
    private void OnDestroy()  // Called when the scene ends by being unloaded in ExampleFinishAfter5Seconds()
    {
        // disable logging
        GameObject.Find("GazeManager").GetComponent<GazeLogger>().StopLogging();

        // deactivate object following gaze
        GameObject.Find("GazeManager").GetComponent<GazeRaycast>().gazeMarkerObj = null;

        // set atom to be finished
        GameObject.Find("StudyManager").GetComponent<StudyManager>().GetCurrentStudyAtom().SetHasFinished();
    }
   
    IEnumerator ExampleFinishAfter10Seconds(int time)
    {
        yield return new WaitForSeconds(time);

        
       
        // finish scene after five seconds
        SceneManager.UnloadSceneAsync(2);
    }


   
}

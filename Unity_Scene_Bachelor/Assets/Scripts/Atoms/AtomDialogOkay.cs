﻿namespace HCI 
{
    public class AtomDialogOkay : AtomDialog
    {
        public AtomDialogOkay(StudyManager studyManager, string message) : base(studyManager, message)
        {
        }

        public override void Start()
        {
            base.Start();

            this.dialogButton.SetActive(true);
            this.SetActive(true);
        }

        public override void OnSliderChange()
        {
        }

        public override void OnButtonPressed()
        {
            this.SetActive(false);
            this.dialogButton.SetActive(false);

            this.hasFinished = true;
        }
    }
}
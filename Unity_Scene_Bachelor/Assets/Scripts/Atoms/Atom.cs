﻿namespace HCI
{
    public abstract class Atom
    {
        protected StudyManager studyManager;

        protected bool hasFinished;

        public Atom(StudyManager studyManager)
        {
            this.studyManager = studyManager;
            this.hasFinished = false;
        }

        public bool GetHasFinished()
        {
            return this.hasFinished;
        }
        
        public void SetHasFinished()
        {
            // External interface to be called by another game object to finish this Atom.

            this.hasFinished = true;
        }

        public abstract void Start();
        public virtual void Update() 
        {
        }
        public abstract void OnSliderChange();
        public abstract void OnButtonPressed();
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

namespace HCI 
{
    public abstract class AtomDialog : Atom
    {
        protected GameObject dialogCanvas;
        protected GameObject dialogText;
        protected GameObject dialogSlider;
        protected GameObject dialogButton;

        protected string message;

        public AtomDialog(StudyManager studyManager, string message) : base(studyManager)
        {
            this.dialogCanvas = studyManager.dialogCanvas;
            this.dialogText = studyManager.dialogText;
            this.dialogSlider = studyManager.dialogSlider;
            this.dialogButton = studyManager.dialogButton;

            this.message = message;
        }

        public override void Start()
        {
            Debug.Log("Set message to: " + this.message);
            this.SetText(this.message);
        }

        protected void SetActive(bool active)
        {
            
            this.dialogCanvas.SetActive(active);
        }

        protected void SetText(string text)
        {
            
            Text textComponent = this.dialogText.GetComponent<Text>();
            textComponent.text = text;
        }
    }
}
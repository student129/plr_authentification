﻿using UnityEngine.UI;

namespace HCI 
{
    public class AtomDialogRange : AtomDialog
    {
        public AtomDialogRange(StudyManager studyManager, string message) : base(studyManager, message)
        {
        }

        public override void Start()
        {
            base.Start();

            this.dialogSlider.SetActive(true);
            this.dialogButton.SetActive(true);

            this.SetActive(true);
        }
        public override void OnSliderChange()
        {
            Slider slider = this.dialogSlider.GetComponent<Slider>();
            this.SetText(message + "\n\nSelected participant: " + (int)slider.value);
        }

        public override void OnButtonPressed()
        {
            this.SetActive(false);

            Slider slider = this.dialogSlider.GetComponent<Slider>();
            this.studyManager.SetParticipant((int)slider.value);

            this.dialogSlider.SetActive(false);
            this.dialogButton.SetActive(false);

            this.hasFinished = true;
        }
    }
}
﻿using UnityEngine.SceneManagement;
using UnityEngine.Video;
using System.Collections;

using UnityEngine;
namespace HCI 
{
    public class AtomScene : Atom
    {
        private int sceneIdx;
        private int nextscene;
        private VideoPlayer videoPlayer;
        public float timer = 0.0f;
        private new GameObject camera;
        public AtomScene(StudyManager studyManager, int sceneIdx) : base(studyManager)
        {
            this.sceneIdx = sceneIdx;
            
        }




        public override void Start()
        {/*
            if (sceneIdx==1)
            {
                camera = GameObject.Find("LeftEye");
                videoPlayer = camera.AddComponent<VideoPlayer>();
                videoPlayer.playOnAwake = false;
                videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;
                videoPlayer.targetCameraAlpha = 0.5F;
                // videoPlayer.url = Application.persistentDataPath + "/Wars.MP4";
                videoPlayer.url = "file://Assets/Resources/Wars.mp4";
                videoPlayer.frame = 100;
                videoPlayer.isLooping = true;
                videoPlayer.Play();
                //  Lightmapping.ClearLightingDataAsset();
                int participantID = GameObject.Find("StudyManager").GetComponent<StudyManager>().GetParticipant() - 95;

                // enable gaze logging
                GameObject.Find("GazeManager").GetComponent<GazeLogger>().StartLogging("StudyvideotestScene_", participantID);

                nextscene = GameObject.Find("StudyManager").GetComponent<StudyManager>().GetNextscene();

                // end scene after some seconds
                //  StartCoroutine(ExampleFinishAfter10Seconds());

            }
            else{
                
                SceneManager.LoadScene(this.sceneIdx, LoadSceneMode.Additive);
            }
            */

            // loaded Scene needs to call HasFinished() to return.
            SceneManager.LoadScene(this.sceneIdx, LoadSceneMode.Additive);
        }

      /*  public override void Update()
        {
            if (sceneIdx == 1)
            {
                timer += Time.deltaTime;
                if (timer >= 10)
                {
                    this.sceneIdx = nextscene;
                    //SceneManager.LoadScene(this.nextscene, LoadSceneMode.Additive);
                    Start();
                }
                else { }

            }



        }*/
        public override void OnSliderChange()
        {
            throw new System.NotImplementedException();
        }

        public override void OnButtonPressed()
        {
            throw new System.NotImplementedException();
        }
        IEnumerator ExampleFinishAfter10Seconds()
        {
            yield return new WaitForSeconds(20);

            // finish scene after five seconds
            SceneManager.UnloadSceneAsync(1);
        }


    }
}
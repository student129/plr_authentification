﻿/**
 * GazeRaycast.cs written by Jonathan Liebers
 */

using System.Collections;
using UnityEngine;

namespace HCI
{
    public class GazeRaycast : MonoBehaviour
    {
        private Pvr_UnitySDKAPI.EyeTrackingGazeRay _gazeRay;

        [HideInInspector]
        public GameObject gazeMarkerObj;  // assigned by script and not in editor
        
        [HideInInspector]
        public bool showGazeMarker = true;

        private RaycastHit _hit;

        // max distance for the raycast
        int maxdistance = 20;

        public void StartGazeRaycasting(GameObject objectNameInScene = null)
        {
            /**
             * Starts the periodic raycasting of gaze. Can optionmally attach a GameObject's transform to the hit of
             * the raycast.
             */
            Debug.Log("Call to GazeRaycast::StartGazeRaycasting() with parameter " + objectNameInScene);

            // do not set the object twice here to prevent race condition.
            if (this.gazeMarkerObj == null && objectNameInScene != null)
            {
                gazeMarkerObj = objectNameInScene;
            }

            StartCoroutine(EyeRaycast());
        }

        private IEnumerator EyeRaycast()
        {
            while (true)
            {
                Pvr_UnitySDKAPI.System.UPvr_getEyeTrackingGazeRay(ref _gazeRay);

                if (_gazeRay.IsValid)
                {
                    // init matrix
                    Transform target = Pvr_UnitySDKManager.SDK.transform;
                    Matrix4x4 mat = Matrix4x4.TRS(target.position, target.rotation, Vector3.one);

                    // transform ray origin
                    _gazeRay.Origin = mat.MultiplyPoint(_gazeRay.Origin);

                    // transform ray direction
                    _gazeRay.Direction = mat.MultiplyVector(_gazeRay.Direction);

                    // create gaze ray
                    Ray ray = new Ray(_gazeRay.Origin, _gazeRay.Direction);
                    
                    if (Physics.Raycast(ray, out _hit, maxdistance))
                    {
                        Debug.Log("############ " + _hit.collider.transform.name);
                        
                        // move the gazeMarkerObj if it is visible and if its set
                        if (showGazeMarker && 
                            gazeMarkerObj != null
                            && ! _hit.collider.transform.name.Equals(gazeMarkerObj.name))
                        {
                            gazeMarkerObj.gameObject.SetActive(true);
                            // gazeMarkerObj.transform.position = _hit.point;
                            gazeMarkerObj.transform.position = new Vector3(_hit.point.x -1, _hit.point.y, _hit.point.z);
                        }
                    }
                    else
                    {
                        if (showGazeMarker && gazeMarkerObj != null)
                        {
                            gazeMarkerObj.gameObject.SetActive(false);
                        }
                    }
                }

                yield return new WaitForSeconds(0);
            }
        }

        public Vector3 GetGazeHitPosition()
        {
            return _hit.point;
        }
        
        public void SetGazeMarkerObject(GameObject gazeMarkerObject)
        {
            gazeMarkerObj = gazeMarkerObject;
        }
    }
}
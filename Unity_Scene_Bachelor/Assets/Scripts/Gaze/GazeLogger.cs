﻿/**
 * GazeLogger.cs written by Jonathan Liebers
 * This file utilizes the Pico SDK (Pvr_UnitySDKAPI) to log basic Gaze information in a CSV that is stored on the 
 * Pico Neo 2 Eye.
 */

using System;
using System.Collections;
using System.IO;
using Tobii.XR;
using UnityEngine;

namespace HCI
{
    public class GazeLogger : MonoBehaviour
    {
        private Pvr_UnitySDKAPI.EyeTrackingData _gazeData;
        private StreamWriter _streamWriter, _streamWriter2;
        private const char Delim = '\t';

        public GameObject head;
        
        public bool enableCoreEyeLogging = true;
        public bool enableOcumenEyeLoggingWithLicense = false;
        
        private bool _enableLogging, _enableLogging2;

        private string _filePath = "";       // Core EyeTracking
        private string _filePath2 = "";      // Ocumen EyeTracking

        void Awake()
        {
            /**
             * The TobiiXR must not be initialized if there is no license file in place.
             * Otherwise the eye tracker will stop working completely and even the PicoXR eye tracking will not work
             * anymore. Thus only activate Ocumen Eye Gaze Logging if a license file is in place and check the logs
             * via `adb logcat -s Unity` if there is some license issue. An example for a logcat output that results
             * in such issues is posted at the end of this file. See this link how the license needs to be added to
             * Unity: https://vr.tobii.com/sdk/develop/tobii-ocumen/advanced-api/#step-2-get-a-license
             */
            
            if (enableOcumenEyeLoggingWithLicense)  
            {
                Debug.Log("Initializing Tobii eyetracker in GazeInitializer::Awake().");
                var settings = new TobiiXR_Settings();
                settings.AdvancedEnabled = true;
                try
                {
                    TobiiXR.Start(settings);
                }
                catch (Exception e)
                {
                    Debug.Log("++ ERROR REPORT START // ERROR IN TobiiXR.Start(settings) with settings " + settings);
                    Debug.Log("Exception message:");
                    Debug.Log(e.Message);
                    Debug.Log("++ ERROR REPORT END");
                }

                Debug.Log("Initializing of Tobii eyetracker in GazeInitializer::Awake() done.");
            }
        }
        
        public void StartLogging(string filename, int participantID)
        {
            filename += '_' + participantID;

                Debug.Log("GazeLogger.cs::StartLogging() called with filename " + filename);
            string directory = Application.persistentDataPath + "/Logs/Eyetracking/";
            Debug.Log("Writing to directory: " + directory);
            
#if UNITY_ANDROID && !UNITY_EDITOR
            Debug.Log("Calling GetComponent<GazeRaycast>().StartGazeRaycasting();");
            GetComponent<GazeRaycast>().StartGazeRaycasting();
            Debug.Log("Called GetComponent<GazeRaycast>().StartGazeRaycasting();");
#endif
            var ctime = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            
            // Create a new CSV file in the application directory
            if (enableCoreEyeLogging || enableOcumenEyeLoggingWithLicense)
            {
                if (!Directory.Exists(directory))
                {
                    Debug.Log("Creating directory: " + directory);
                    Directory.CreateDirectory(directory);
                }
            }

            if (enableCoreEyeLogging)
            {

                string filePath = directory + "GazeData_" + (Int32) ctime + "_" + filename + ".tsv";
                this._filePath = filePath;
                Debug.Log("Gaze Logging File: " + filePath);

                _streamWriter = new StreamWriter(filePath);

                // header
                _streamWriter.WriteLine("Unity.frameCount" + Delim +
                                        "Unity.realtimeSinceStartup" + Delim +

                                        "Unity.Head.position.x" + Delim +
                                        "Unity.Head.position.y" + Delim +
                                        "Unity.Head.position.z" + Delim +
                                        "Unity.Head.rotation.euler_x" + Delim +
                                        "Unity.Head.rotation.euler_y" + Delim +
                                        "Unity.Head.rotation.euler_z" + Delim +

                                        "Unity.gazeHit.position.x" + Delim +
                                        "Unity.gazeHit.position.y" + Delim +
                                        "Unity.gazeHit.position.z" + Delim +
                                        "PicoXR.gazeHit.position.x" + Delim +
                                        "PicoXR.gazeHit.position.y" + Delim +
                                        "PicoXR.gazeHit.position.z" + Delim +

                                        "gazeData.leftEyePoseStatus" + Delim +
                                        "gazeData.rightEyePoseStatus" + Delim +
                                        "gazeData.combinedEyePoseStatus" + Delim +

                                        "gazeData.leftEyeGazePoint.x" + Delim +
                                        "gazeData.leftEyeGazePoint.y" + Delim +
                                        "gazeData.leftEyeGazePoint.z" + Delim +

                                        "gazeData.rightEyeGazePoint.x" + Delim +
                                        "gazeData.rightEyeGazePoint.y" + Delim +
                                        "gazeData.rightEyeGazePoint.z" + Delim +

                                        "gazeData.combinedEyeGazePoint.x" + Delim +
                                        "gazeData.combinedEyeGazePoint.y" + Delim +
                                        "gazeData.combinedEyeGazePoint.z" + Delim +

                                        "gazeData.combinedEyeGazePoint.x" + Delim +
                                        "gazeData.combinedEyeGazePoint.y" + Delim +
                                        "gazeData.combinedEyeGazePoint.z" + Delim +

                                        "gazeData.leftEyeOpenness" + Delim +
                                        "gazeData.rightEyeOpenness" + Delim +

                                        "gazeData.leftEyePupilDilation" + Delim +
                                        "gazeData.rightEyePupilDilation" + Delim +

                                        "gazeData.leftEyePositionGuide.x" + Delim +
                                        "gazeData.leftEyePositionGuide.y" + Delim +
                                        "gazeData.leftEyePositionGuide.z" + Delim +

                                        "gazeData.rightEyePositionGuide.x" + Delim +
                                        "gazeData.rightEyePositionGuide.y" + Delim +
                                        "gazeData.rightEyePositionGuide.z" + Delim +

                                        "gazeData.foveatedGazeDirection.x" + Delim +
                                        "gazeData.foveatedGazeDirection.y" + Delim +
                                        "gazeData.foveatedGazeDirection.z" + Delim +

                                        "gazeData.foveatedGazeTrackingState"
                );

                _streamWriter.Flush();
                _enableLogging = true;
            }

            if (enableOcumenEyeLoggingWithLicense)
            {
                string filePath2 = directory + "GazeDataOcumen_" + (Int32) ctime + "_" + filename + ".json";
                this._filePath2 = filePath2;
                Debug.Log("Gaze Logging File (Ocumen): " + filePath2);

                _streamWriter2 = new StreamWriter(filePath2);
                _streamWriter2.WriteLine("[");
                _enableLogging2 = true;
            }

#if UNITY_ANDROID && !UNITY_EDITOR
            if (enableCoreEyeLogging)
            {
                Debug.Log("StartCoroutine(EyeRaycastLog());");
                StartCoroutine(EyeRaycastLog());
            }

            if (enableOcumenEyeLoggingWithLicense)
            {
                Debug.Log("StartCoroutine(OcumenLog());");
                StartCoroutine(OcumenLog());
            }
#endif
        }

        IEnumerator EyeRaycastLog()
        {
            while (_enableLogging)
            {
                if (Pvr_UnitySDKAPI.System.UPvr_getEyeTrackingData(ref _gazeData))
                {
                    try
                    {
                        var headposition = head.transform.position;
                        var headrotation = head.transform.rotation;
                        var gazeHitPosition = GetComponent<GazeRaycast>().GetGazeHitPosition();
                        var picoHitPosition = head.GetComponent<Pvr_UnitySDKEyeManager>().GetEyeTrackingPos();

                        string line = "" +
                                      Time.frameCount + Delim +
                                      Time.realtimeSinceStartup + Delim +

                                      headposition.x + Delim +
                                      headposition.y + Delim +
                                      headposition.z + Delim +
                                      headrotation.eulerAngles.x + Delim +
                                      headrotation.eulerAngles.y + Delim +
                                      headrotation.eulerAngles.z + Delim +

                                      gazeHitPosition.x + Delim +
                                      gazeHitPosition.y + Delim +
                                      gazeHitPosition.z + Delim +
                                      picoHitPosition.x + Delim +
                                      picoHitPosition.y + Delim +
                                      picoHitPosition.z + Delim +

                                      _gazeData.leftEyePoseStatus + Delim +
                                      _gazeData.rightEyePoseStatus + Delim +
                                      _gazeData.combinedEyePoseStatus + Delim +

                                      _gazeData.leftEyeGazePoint.x + Delim +
                                      _gazeData.leftEyeGazePoint.y + Delim +
                                      _gazeData.leftEyeGazePoint.z + Delim +

                                      _gazeData.rightEyeGazePoint.x + Delim +
                                      _gazeData.rightEyeGazePoint.y + Delim +
                                      _gazeData.rightEyeGazePoint.z + Delim +

                                      _gazeData.combinedEyeGazePoint.x + Delim +
                                      _gazeData.combinedEyeGazePoint.y + Delim +
                                      _gazeData.combinedEyeGazePoint.z + Delim +

                                      _gazeData.combinedEyeGazePoint.x + Delim +
                                      _gazeData.combinedEyeGazePoint.y + Delim +
                                      _gazeData.combinedEyeGazePoint.z + Delim +

                                      _gazeData.leftEyeOpenness + Delim +
                                      _gazeData.rightEyeOpenness + Delim +

                                      _gazeData.leftEyePupilDilation + Delim +
                                      _gazeData.rightEyePupilDilation + Delim +

                                      _gazeData.leftEyePositionGuide.x + Delim +
                                      _gazeData.leftEyePositionGuide.y + Delim +
                                      _gazeData.leftEyePositionGuide.z + Delim +

                                      _gazeData.rightEyePositionGuide.x + Delim +
                                      _gazeData.rightEyePositionGuide.y + Delim +
                                      _gazeData.rightEyePositionGuide.z + Delim +

                                      _gazeData.foveatedGazeDirection.x + Delim +
                                      _gazeData.foveatedGazeDirection.y + Delim +
                                      _gazeData.foveatedGazeDirection.z + Delim +

                                      _gazeData.foveatedGazeTrackingState;
                        _streamWriter.WriteLine(line);

                        _streamWriter.Flush();
                    }
                    catch (NullReferenceException e)
                    {
                        _streamWriter.WriteLine(e.Message);  // write exception to log
                        Debug.Log(e.Message);
                        continue;
                    }
                }

                yield return new WaitForSeconds(0);
            }
        }
        
        IEnumerator OcumenLog()
        {
            Debug.Log("Call to GazeLogger::OcumenLog().");
            bool receivedData = false;
            
            while (this._enableLogging2)
            {
                if (TobiiXR.Advanced == null)
                {
                    Debug.Log("+++ CRITICAL ERROR +++ TobiiXR.Advanced is null! Ending OcumenLog().");
                    break;
                } else if (TobiiXR.Advanced.QueuedData == null) {
                    Debug.Log("+++ CRITICAL ERROR +++ TobiiXR.Advanced.QueuedData is null! Ending OcumenLog().");
                    break;
                }
                
                // Dequeue all new data received since last frame
                while (TobiiXR.Advanced.QueuedData.Count > 0)
                {
                    TobiiXR_AdvancedEyeTrackingData data = TobiiXR.Advanced.QueuedData.Dequeue();

                    // Serialize to json and append to file
                    if (receivedData) _streamWriter2.WriteLine(",");
                    var json = JsonUtility.ToJson(data);
                    _streamWriter2.Write(json);
                    receivedData = true;
                }

                Debug.Log("TobiiXR.Advanced.QueuedData.Count " + TobiiXR.Advanced.QueuedData.Count);
                yield return new WaitForSeconds(0);
            }
        }

        public void writeMarker()
        {
            if (_streamWriter2 != null)
            {
                _streamWriter2.WriteLine("{ 'info': 'grau szene ist jetzt zu ende'},");
                _streamWriter2.Flush();
            }
        }

        public void StopLogging()
        {
            if (enableCoreEyeLogging)
            {
                _enableLogging = false;
                Debug.Log("Gaze Logging File closed: " + this._filePath);
                _streamWriter.Close();
            }
            
            if (enableOcumenEyeLoggingWithLicense)
            {
                _enableLogging2 = false;
                _streamWriter2.WriteLine("]");
                Debug.Log("Gaze Logging File (Ocumen) closed: " + this._filePath2);
                _streamWriter2.Close();
            }
        }
    }
}



/*
 * Example output of an ocumen licensing issue (Jonathan Liebers, 2021-01-19):
 * If such an issue exists, the eye tracker did not find a Tobii Ocumen License and stops working completely. 
 *
 * 01-19 14:50:41.492  6796  6826 E Unity   : License 0 failed. Return code TOBII_LICENSE_VALIDATION_RESULT_INVALID_PROCESS_NAME
 * 01-19 14:50:41.492  6796  6826 E Unity   : Tobii.XR.ConnectionHelper:CreateDeviceContext(IStreamEngineInterop, String, tobii_field_of_use_t, IntPtr, String[], IntPtr&)
 * 01-19 14:50:41.492  6796  6826 E Unity   : Tobii.XR.ConnectionHelper:GetFirstSupportedTracker(IStreamEngineInterop, IntPtr, IList`1, StreamEngineTracker_Description, IntPtr&, String&)
 * 01-19 14:50:41.492  6796  6826 E Unity   : Tobii.XR.ConnectionHelper:TryConnect(IStreamEngineInterop, StreamEngineTracker_Description, StreamEngineContext&, tobii_custom_log_t)
 * 01-19 14:50:41.492  6796  6826 E Unity   : Tobii.XR.StreamEngineTracker:.ctor(StreamEngineTracker_Description)
 * 01-19 14:50:41.492  6796  6826 E Unity   : Tobii.XR.TobiiProvider:Initialize(StreamEngineTracker_Description)
 * 01-19 14:50:41.492  6796  6826 E Unity   : Tobii.XR.TobiiXR:Start(TobiiXR_Settings)
 * 01-19 14:50:41.492  6796  6826 E Unity   : HCI.GazeLogger:Awake()
 * 01-19 14:50:41.492  6796  6826 E Unity   :  
 * 01-19 14:50:41.492  6796  6826 E Unity   : (Filename: ./Runtime/Export/Debug.bindings.h Line: 45)
 * 01-19 14:50:41.492  6796  6826 E Unity   : 
 * 01-19 14:50:41.499  6796  6826 I Unity   : Connected to SE tracker: tobii-prp://1359101137 and it took 231ms
 * 01-19 14:50:41.499  6796  6826 I Unity   : Tobii.XR.ConnectionHelper:TryConnect(IStreamEngineInterop, StreamEngineTracker_Description, StreamEngineContext&, tobii_custom_log_t)
 * 01-19 14:50:41.499  6796  6826 I Unity   : Tobii.XR.StreamEngineTracker:.ctor(StreamEngineTracker_Description)
 * 01-19 14:50:41.499  6796  6826 I Unity   : Tobii.XR.TobiiProvider:Initialize(StreamEngineTracker_Description)
 * 01-19 14:50:41.499  6796  6826 I Unity   : Tobii.XR.TobiiXR:Start(TobiiXR_Settings)
 * 01-19 14:50:41.499  6796  6826 I Unity   : HCI.GazeLogger:Awake()
 * 01-19 14:50:41.499  6796  6826 I Unity   :  
 * 01-19 14:50:41.499  6796  6826 I Unity   : (Filename: ./Runtime/Export/Debug.bindings.h Line: 45)
 * 01-19 14:50:41.499  6796  6826 I Unity   : 
 * 01-19 14:50:41.501  6796  6826 I Unity   : ERROR [prp-client] "PRP_ERROR_ENUM_INSUFFICIENT_LICENSE (00000011)" {FileName:"prp_client.cpp(1057)",Function:"operator()",Tags:["PRP"]}
 * 01-19 14:50:41.501  6796  6826 I Unity   :  
 * 01-19 14:50:41.501  6796  6826 I Unity   : (Filename: ./Runtime/Export/Debug.bindings.h Line: 45)
 * 01-19 14:50:41.501  6796  6826 I Unity   : 
 * 01-19 14:50:41.502  6796  6826 I Unity   : internal_device.cpp(2634): error "TOBII_ERROR_INSUFFICIENT_LICENSE" (00000002) in function "device_compound_stream_subscribe"
 * 01-19 14:50:41.502  6796  6826 I Unity   :  
 * 01-19 14:50:41.502  6796  6826 I Unity   : (Filename: ./Runtime/Export/Debug.bindings.h Line: 45)
 * 01-19 14:50:41.502  6796  6826 I Unity   : 
 * 01-19 14:50:41.502  6796  6826 I Unity   : internal.cpp(343): error "TOBII_ERROR_INSUFFICIENT_LICENSE" (00000002) in function "tobii_compound_stream_subscribe"
 * 01-19 14:50:41.502  6796  6826 I Unity   :  
 * 01-19 14:50:41.502  6796  6826 I Unity   : (Filename: ./Runtime/Export/Debug.bindings.h Line: 45)
 * 01-19 14:50:41.502  6796  6826 I Unity   : 
 * 01-19 14:50:41.502  6796  6826 I Unity   : tobii_wearable.cpp(110): error "TOBII_ERROR_INSUFFICIENT_LICENSE" (00000002) in function "tobii_wearable_advanced_data_subscribe"
 * 01-19 14:50:41.502  6796  6826 I Unity   :  
 * 01-19 14:50:41.502  6796  6826 I Unity   : (Filename: ./Runtime/Export/Debug.bindings.h Line: 45)
 * 01-19 14:50:41.502  6796  6826 I Unity   : 
 * 01-19 14:50:41.507  6796  6826 I Unity   : Failed to subscribe to eye tracking data: TOBII_ERROR_INSUFFICIENT_LICENSE
 * 01-19 14:50:41.507  6796  6826 I Unity   : Tobii.XR.TobiiProvider:Initialize(StreamEngineTracker_Description)
 * 01-19 14:50:41.507  6796  6826 I Unity   : Tobii.XR.TobiiXR:Start(TobiiXR_Settings)
 * 01-19 14:50:41.507  6796  6826 I Unity   : HCI.GazeLogger:Awake()
 */
## Readme is still WIP. Ask Jonathan instead. :-)

Set your SDK and NDK in Unity:
Edit -> Preferences -> Extermal Tools
* SDK and NDK paths must be set. Click the "Download"-button to download the SDK or NDK.

### Unity Version

* Required version: **2018.4.23f1** with Android Build Support
* Unity Hub download link: unityhub://2018.4.23f1/c9cf1a90e812
* Unity Download Archive: https://unity3d.com/get-unity/download/archive

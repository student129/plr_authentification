* GazeData files with 'Ocumen' in the filename are created by Tobii Ocument and require a valid license within the app.
* GazeData files without 'Ocumen' in the filename can be created by the default Pico firmware.
* TSV stands for 'tabulator seperated values'.
* The 'Ocumen'-files are not actually TSV files, but contain a list of JSONs instead.

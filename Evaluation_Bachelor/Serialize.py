import pandas as pd
import numpy as np
from tqdm import tqdm
from glob import glob
import pickle
import re


# method eto select only usable variable form dataframe
def transform_to_Pandas(data_array):
    df = pd.DataFrame(columns=['Device_time', 'System_time', 'Is_blinking', 'pupil_diameter', 'new_time'])
    time = 0.0
    for data in data_array:
        time += 0.001
        try:
            df = df.append({'Device_time': data[0],
                            'System_time': data[2],
                            'Is_blinking': str(str(data[1]).split("'IsBlinking': ")[1][:5]),
                            'pupil_diameter': float(
                                re.findall("\d+\.\d+", (str(data[1]).split("'PupilDiameter': ")[1][:17]))[0]),
                            'grau_finish': 'no'}, ignore_index=True)
        except IndexError:
            print(str(data))

    return df


# set all data in dictionary in np array form and sve it in txt file
def Serialize_all():
    for g in tqdm(glob('*.json')):
        print(g)
        try:
            current_df = pd.read_json(g)

            identity = str(int(g[-10:-8])).rjust(2, '0')

            # identity = int(g[-10:-8])
            print('identity ist' + str(identity))

            npleft = np.array(current_df[['DeviceTimestamp', 'Left', 'SystemTimestamp']])
            npright = np.array(current_df[['DeviceTimestamp', 'Right', 'SystemTimestamp']])

            data_dict = {'Left': transform_to_Pandas(npleft),
                         'Right': transform_to_Pandas(npright)}

            scene = int(g.split('StudyScene')[1][:1])

            session = int(g[-7:-5])

            filename = 'Pb' + str(identity) + '_S' + str(scene) + '_ss' + str(session) + '.txt'
            file = open(filename, 'wb')

            pickle.dump(data_dict, file)

            file.close()
        except ValueError:
            print(g)


DATASET = []


# set all Data from txt file in Dict DATASET
# And add new column to prepare data for machine learning and deep learning

def Deserialize_all(is_filter=True, rolling=20):
    for g in tqdm(glob('*.txt')):
        file = open(g, 'rb')
        # dump information to that file
        daten = pickle.load(file)

        # close the file
        file.close()

        df_left = (daten['Left'])
        df_right = (daten['Right'])

        #  select only usable data
        data_left = df_left.loc[df_left['Is_blinking'] == 'False']
        data_right = df_right.loc[df_right['Is_blinking'] == 'False']

        if not is_filter:
            data_left = df_left
            data_right = df_right

        data_left.System_time -= data_left.System_time.iloc[0]
        data_right.System_time -= data_right.System_time.iloc[0]
        # add column sub_operation
        data_left['sub_operation'] = data_left.pupil_diameter - data_right.pupil_diameter
        data_right['sub_operation'] = data_left.pupil_diameter - data_right.pupil_diameter
        # add column add_operation
        data_left['add_operation'] = data_left.pupil_diameter + data_right.pupil_diameter
        data_right['add_operation'] = data_left.pupil_diameter + data_right.pupil_diameter

        # set NaN to null
        data_left['add_operation'] = data_left['add_operation'].fillna(0)
        data_right['add_operation'] = data_right['add_operation'].fillna(0)
        # add column diameter_median_filtered
        data_left['diameter_median_filtered'] = data_left.pupil_diameter.rolling(rolling).median()
        data_right['diameter_median_filtered'] = data_right.pupil_diameter.rolling(rolling).median()

        count_false_left = df_left.Is_blinking.value_counts()
        count_false_right = df_right.Is_blinking.value_counts()

        identity = int(g.split('Pb')[1][:2])
        # add to dictionary
        try:
            data_dict = {'path': g,
                         'identity': identity,
                         'session': int(g[-5:-4]),
                         'scene': int(g.split('S')[1][:1]),
                         'left_false_Percent': (count_false_left[0] / len(df_left)) * 100,
                         'right_false_Percent': (count_false_right[0] / len(df_right)) * 100,
                         'df_Left': data_left,
                         'df_Right': data_right

                         }

            DATASET.append(data_dict)
        except IndexError:
            print(g.split('S'))


# return dataset with all data
def get_Dataset(is_filter=True, rolling=20):
    Deserialize_all(is_filter, rolling)
    return DATASET

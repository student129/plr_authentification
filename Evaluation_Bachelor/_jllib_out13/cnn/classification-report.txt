Classification Report
              precision    recall  f1-score   support

           0       0.03      0.01      0.02       776
           1       0.28      0.65      0.39       718
           2       0.03      0.02      0.02       797
           3       0.00      0.00      0.00       806
           4       0.07      0.04      0.05       743
           5       0.00      0.00      0.00       519
           6       0.17      0.16      0.16       800
           7       0.20      0.15      0.17       776
           8       0.25      0.29      0.27       804
           9       0.10      0.28      0.15       790
          10       0.22      0.08      0.12       693
          11       0.41      0.67      0.51       775
          12       0.12      0.01      0.02       804
          13       0.20      0.53      0.29       804
          14       0.01      0.00      0.00       634
          15       0.17      0.19      0.18       722
          16       0.21      0.11      0.14       742
          17       0.00      0.00      0.00       797
          18       0.12      0.07      0.09       784
          19       0.06      0.13      0.08       731

    accuracy                           0.17     15015
   macro avg       0.13      0.17      0.13     15015
weighted avg       0.14      0.17      0.14     15015

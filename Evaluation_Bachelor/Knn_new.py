from Prepare_class import Prepare
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
import seaborn


def euclidean_dist(x1, x2, y1, y2):
    return ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5


def get_Min(array):
    result = []
    mini = array[0]['distance']
    # print(type(mini))
    person_id = array[0]['person_id']
    for j in range(0, len(array)):
        if mini > float(array[j]['distance']):
            mini = array[j]['distance']
            person_id = array[j]['person_id']
    return mini, person_id


class knn:
    data_session1 = []
    data_session2 = []
    array_threshold = []
    scene_id = 0

    def __init__(self, dataset, eye_side, scene_id, array_threshold):
        prepare = Prepare(dataset)
        self.data_session1, self.data_session2 = prepare.get_db_and_newdata(scene_id, eye_side)
        self.array_threshold = array_threshold
        self.scene_id = scene_id

    def run_knn(self) -> float:

        array_pp = []
        arr_confusion = []
        for threshold in self.array_threshold:

            dict_result = []
            dict_rate = []
            tp, tn, fp, fn = 0, 0, 0, 0
            print("new tre ist " + str(threshold))
            for d1 in self.data_session1:

                current_data = d1['left_right']
                id_person = current_data['identity']
                data_person = np.array(current_data['df_Left'][['System_time', 'add_operation']])
                # print(current_data)
                # print(current_data['df_Right'])

                # break
                distance_euclide = 0
                array_distance = []
                db_id_person = 0
                for d2 in self.data_session2:
                    db_data = d2['left_right']
                    db_id_person = db_data['identity']
                    db_person = np.array(db_data['df_Left'][['System_time', 'add_operation']])
                    # print(db_person)

                    way = len(data_person)
                    if way > len(db_person):
                        way = len(db_person)
                    # print(way)
                    distance_euclide = 0

                    for i in range(0, way):
                        distance_euclide += euclidean_dist(data_person[i][0], db_person[i][0], data_person[i][1],
                                                           db_person[i][1])

                    # print('person ' + str(db_id_person) + ' has distance ' + str(distance_euclide))
                    array_distance.append({'distance': distance_euclide, 'person_id': db_id_person})

                mini, id = get_Min(array_distance)
                arr_confusion.append(id)

                print('for curent person' + str(id_person) + ' the authenticate person is person ' + str(
                   id) + 'with a '
                        'distance of ' + str(mini))

                if mini < threshold and id == id_person:
                    tp += 1
                elif mini < threshold and id != id_person:
                    fp += 1
                elif mini > threshold and id not in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]:
                    fn += 1
                elif mini > threshold and id in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]:
                    tn += 1
                    # print({'fp': fp, 'tn': tn, 'fn': fn, 'tp': tp})

            # print({'tp': tp, 'fp': fp, 'tn': tn,  'fn': fn, 'threshold': threshold, 'scene_id': self.scene_id})
            try:
                array_pp.append(
                    {'tp': tp, 'fp': fp, 'tn': tn, 'fn': fn, 'threshold': threshold,
                     'Accuracy': (tp + tn) / (tp + fp + fn + tn), 'precision': tp / (tp + fp),
                     'recall': tp / (tp + fn), 'scene_id': self.scene_id})

            except ZeroDivisionError:
                print('tp_' + str(tp) + 'fp_' + str(fp) + 'tn_' + str(tn) + 'fn' + str(fn))
            # break

        # get best accuracy, recall and precision
        best_accuracy = array_pp[0]['Accuracy']
        recall = array_pp[0]['recall']
        precision = array_pp[0]['precision']
        for i in range(0, len(array_pp)):
            if precision < array_pp[i]['precision']:
                best_accuracy = array_pp[i]['Accuracy']
                recall = array_pp[i]['recall']
                precision = array_pp[i]['precision']
        # print(best_accuracy)
        # print(recall)
        # print(precision)

        # create fnr fpr graph
        arr_threshold = [array_pp[i]['threshold'] for i in range(0, len(array_pp))]
        arr_x = [(array_pp[i]['fp'] / (array_pp[i]['fp'] + array_pp[i]['tn'])) for i in range(0, len(array_pp))]
        arr_y = [(array_pp[i]['fn'] / (array_pp[i]['fn'] + array_pp[i]['tp'])) for i in range(0, len(array_pp))]

        fig = plt.figure(figsize=(10, 10))
        plt.plot(arr_threshold, arr_x, label="FNR eye")
        plt.plot(arr_threshold, arr_y, label="FPR eye")
        plt.xlabel('threshold')
        plt.ylabel('Rate')
        plt.legend(loc="upper left")
        fig.savefig("knn_scene_" + str(self.scene_id) + "FN and fpr and best_accuracy_" + str(best_accuracy) + ".pdf")
        plt.show()

        labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
                  "17", "18", "19", "20"]
        # labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"]
        data = []
        for i in range(0, 20):
            p = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            # print(i)
            # print(arr_confusion)
            p[arr_confusion[i]] = 1
            data.append(p)
        # print(data)
        # create confusion matrix
        seaborn.set(color_codes=True)
        plt.figure(1, figsize=(10, 10))
        plt.title("knn_confusion_matrix in scene_" + str(self.scene_id) + "best_accuracy_" + str(best_accuracy))
        seaborn.set(font_scale=1.4)
        ax = seaborn.heatmap(data, annot=True, cmap="Blues", cbar_kws={'label': 'Scale'})
        ax.set_xticklabels(labels)
        ax.set_yticklabels(labels)

        ax.set(ylabel="True Probanden", xlabel="Predicted Probanden")
        plt.savefig("knn_confusion_matrix" + str(self.scene_id) + ".pdf", bbox_inches='tight', dpi=300)
        plt.show()

        return best_accuracy

# try:
# print({'fp': fp, 'tn': tn, 'fn': fn, 'tp': tp})
#   accuracy = (tp + tn) / (tp + tn + fn + fp)
#  print(str(accuracy) + ' is the accuracy')
# FNR = fp / fp + tn
# FPR = fn / fn + tp
# dict_rate.append([FNR, FPR, self.threshold])
# except ZeroDivisionError:
#   print('div null')

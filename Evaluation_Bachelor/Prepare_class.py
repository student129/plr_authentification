import matplotlib.pyplot as plt
import re
import numpy as np
from tqdm import tqdm


# class for data preparation for machine learning und deep Learning
# plot all data in scene
class Prepare:
    dataset = []

    def __init__(self, dataset):
        self.dataset = dataset

    # plot data one person for twice session
    def Plot_2session_for_one(self, Id_person, eye_side, scene):
        eye1 = []
        eye2 = []
        if eye_side == 'Left':
            Df_name = 'df_Left'
        else:
            Df_name = 'df_Right'
        for data in self.dataset:

            if data['scene'] == scene and data['session'] == 1 and data['identity'] == Id_person:
                eye1 = np.array(data[Df_name])

        for data in self.dataset:
            if data['scene'] == scene and data['session'] == 2 and data['identity'] == Id_person:
                eye2 = np.array(data[Df_name])

        x = [eye1[i][1] for i in range(0, len(eye1))]
        y = [eye1[i][3] for i in range(0, len(eye1))]

        x1 = [eye2[i][1] for i in range(0, len(eye2))]
        y1 = [eye2[i][3] for i in range(0, len(eye2))]

        plt.scatter(x, y, label="Person_" + str(Id_person) + 'sc_' + str(scene) + 'eye_'
                                + str(eye_side) + 'se_' + str(1), s=10)
        plt.scatter(x1, y1, label="Person_" + str(Id_person) + 'sc_' + str(scene) + 'eye_'
                                  + str(eye_side) + 'se' + str(2), s=10)
        plt.xlabel('timestamp')
        plt.ylabel('diameter')
        plt.legend(loc="upper left")
        plt.savefig("Person_" + str(Id_person) + 'scene' + str(scene) + 'eye_side'
                    + str(eye_side) + 'session' + str(2) + '.pdf')
        plt.show()

    # plot all data for one session
    def plot_all_for_1session(self, eye_side, session, scene):
        Array_sort = []
        Df_name = 'df_Right'
        if eye_side == 'Left':
            Df_name = 'df_Left'

        for data in self.dataset:
            if data['scene'] == scene and data['session'] == session:
                Array_sort.append(np.array(data[Df_name]))
        plt.title("all in " + str(scene) + " for eye_" + eye_side + 'session_' + str(session))
        count = 0
        for person in tqdm(Array_sort):
            count += 1
            x = [person[i][1] for i in range(0, len(person))]
            y = [person[i][3] for i in range(0, len(person))]
            # y = [re.findall("\d+\.\d+", person[i][3]) for i in range(0, len(person))]
            plt.scatter(x, y, label="Person_" + str(count) + 'sc_' + str(scene) + 'eye_'
                                    + str(eye_side) + 'se_' + str(session), s=0.2)
            # , s=0.2
            # if count >= 6:
            #  break
        # , markevery=5
        plt.xlabel('timestamp')
        plt.ylabel('diameter')
        # plt.legend(loc="upper left")
        plt.savefig("All Person in" + 'scene' + str(scene) + 'eye_side'
                    + str(eye_side) + 'session' + str(session))
        plt.show()

    # separated data first from second session
    def get_db_and_newdata(self, scene_id, eye_side):
        session_1 = []
        session_2 = []
        df_name = 'left_right'

        for dates in self.dataset:
            if dates['scene'] == scene_id:
                if int(dates['session']) == 1 and dates['scene'] == scene_id:
                    session_1.append({df_name: dates})
                    # session_1.append({df_name: dates[df_name]})

                elif int(dates['session']) == 2 and dates['scene'] == scene_id:
                    session_2.append({df_name: dates})
                    # session_2.append({df_name: (dates[df_name])})

        return session_1, session_2

    # built boxplot for first 10 person
    def build_boxplot(self, eye_side, scene_id, session):
        diction = {}
        for dat in self.dataset:
            if dat['scene'] == scene_id and dat['session'] == session:
                diction[dat['identity']] = np.array(dat[eye_side].head(100)['add_operation'])
        fig, ax = plt.subplots()
        plt.xlabel('Probanden')
        plt.ylabel('diameter')
        plt.title("boxplot all in scene_" + str(scene_id) + " for eye_" + eye_side + '_session_' + str(session))
        ax.boxplot(diction.values())
        ax.set_xticklabels(diction.keys(), rotation=20)
        plt.savefig("boxplot Person in" + 'scene' + str(scene_id) + '_eye_side'
                    + str(eye_side) + 'session' + str(session) + '.pdf')

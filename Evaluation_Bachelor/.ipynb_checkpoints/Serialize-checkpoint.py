import pandas as pd
import numpy as np
from tqdm import tqdm
from glob import glob
import pickle
import re


# new time please not forgotten to add it
def transform_to_Pandas(data_array):
    df = pd.DataFrame(columns=['Device_time', 'System_time', 'Is_blinking', 'pupil_diameter', 'new_time'])
    time = 0.0
    for data in data_array:
        time += 0.001
        try:
            df = df.append({'Device_time': data[0],
                            'System_time': data[2],
                            'Is_blinking': str(str(data[1]).split("'IsBlinking': ")[1][:5]),
                            'pupil_diameter': float(
                                re.findall("\d+\.\d+", (str(data[1]).split("'PupilDiameter': ")[1][:17]))[0]),
                            'grau_finish': 'no'}, ignore_index=True)
        except IndexError:
            print(str(data[1]))

    return df


def Serialize_all():
    for g in tqdm(glob('*.json')):
        print(g)
        current_df = pd.read_json(g)

        npleft = np.array(current_df[['DeviceTimestamp', 'Left', 'SystemTimestamp']])
        npright = np.array(current_df[['DeviceTimestamp', 'Right', 'SystemTimestamp']])

        data_dict = {'Left': transform_to_Pandas(npleft),
                     'Right': transform_to_Pandas(npright)}
        print(data_dict)
        # (g.split('GazeDataOcumen_')[1][:1])
        scene = int(g.split('StudyScene')[1][:1])
        print(scene)
        session = int(g[-7:-5])
        print(session)

        identity = int(g[-9:-8])
        print(identity)
        filename = 'Pb' + str(identity) + '_S' + str(scene) + '_ss' + str(session) + '.txt'
        file = open(filename, 'wb')
        # dump information to that file
        pickle.dump(data_dict, file)
        # pickle.dump(data_dict, file)

        # close the file
        file.close()

DATASET = []
def Deserialize_all(is_filter=True, rolling=20):
    
    for g in tqdm(glob('*.txt')):
        file = open(g, 'rb')
        # dump information to that file
        daten = pickle.load(file)

        # close the file
        file.close()
        # array_left = np.array(daten['Left'])
        # array_right = np.array(daten['Right'])

        df_left = (daten['Left'])
        df_right = (daten['Right'])

        data_left = df_left.loc[df_left['Is_blinking'] == 'False']
        data_right = df_right.loc[df_right['Is_blinking'] == 'False']

        # df_left = pd.DataFrame(array_left, columns={'Device_time', 'System_time', 'Is_blinking', 'pupil_diameter',
        #                                            'new_time'})
        # df_right = pd.DataFrame(array_right, columns={'Device_time', 'System_time', 'Is_blinking', 'pupil_diameter',
        #                                              'new_time'})
        # print(data_left)
        if not is_filter:
            data_left = df_left
            data_right = df_right

        data_left.System_time -= data_left.System_time.iloc[0]
        data_right.System_time -= data_right.System_time.iloc[0]

        data_left['sub_operation'] = data_left.pupil_diameter - data_right.pupil_diameter
        data_right['sub_operation'] = data_left.pupil_diameter - data_right.pupil_diameter

        data_left['add_operation'] = data_left.pupil_diameter + data_right.pupil_diameter
        data_right['add_operation'] = data_left.pupil_diameter + data_right.pupil_diameter

        data_left['diameter_median_filtered'] = data_left.pupil_diameter.rolling(rolling).median()
        data_right['diameter_median_filtered'] = data_right.pupil_diameter.rolling(rolling).median()

        count_false_left = df_left.Is_blinking.value_counts()
        count_false_right = df_right.Is_blinking.value_counts()

        identity = int(g.split('Pb')[1][:1])
        try:
            data_dict = {'path': g,
                         'identity': identity,
                         'session': int(g[-5:-4]),
                         'scene': int(g.split('S')[1][:1]),
                         'left_false_Percent': (count_false_left[0] / len(df_left)) * 100,
                         'right_false_Percent': (count_false_right[0] / len(df_right)) * 100,
                         'df_Left': data_left,
                         'df_Right': data_right

                         }

            DATASET.append(data_dict)
        except IndexError:
            print(g.split('S'))
      


def get_Dataset(is_filter=True, rolling=20):
    Deserialize_all(is_filter, rolling)
    return DATASET

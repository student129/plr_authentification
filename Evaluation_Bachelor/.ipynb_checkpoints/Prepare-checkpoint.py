import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn


def count_true(arr):
    count = 0
    for i in range(0, len(arr)):
        current_line = arr[i][1]['PupilDiameterValid']
        if current_line:
            count = count + 1
    return count


def plot_confusion_matrix(data, labels, scene, eyeid, methode):
    seaborn.set(color_codes=True)
    plt.figure(1, figsize=(5, 5))
    plt.title("confusion_matrix" + str(scene) + "_" + str(eyeid))
    seaborn.set(font_scale=1.4)
    ax = seaborn.heatmap(data, annot=True, cmap="Blues", cbar_kws={'label': 'Scale'})
    ax.set_xticklabels(labels)
    ax.set_yticklabels(labels)
    ax.set(ylabel="True Probanden", xlabel="Predicted Probanden")
    plt.savefig("confusion_matrix" + str(scene) + "methode_" + methode + 'eye_' + str(eyeid) + ".pdf",
                bbox_inches='tight', dpi=300)
    plt.show()
    # plt.close()
    # plt.show()


def plot_scene4(person1_0, person1_1):
    x = [person1_0[i][1] for i in range(0, len(person1_0))]
    y = [person1_0[i][0]['PupilDiameter'] for i in range(0, len(person1_0))]
    x1 = [person1_1[i][1] for i in range(0, len(person1_1))]
    y1 = [person1_1[i][0]['PupilDiameter'] for i in range(0, len(person1_1))]
    plt.scatter(x, y, label="Person_" + str("person_0"))
    plt.scatter(x1, y1, label="Person_" + str("person_1"))
    plt.xlabel('timestamp')
    plt.ylabel('diameter')
    plt.legend(loc="upper left")
    plt.show()


def filter_data(datajson):
    df = pd.read_json(datajson)
    arrayLeft = pd.DataFrame(df, columns=['Left', 'DeviceTimestamp', 'SystemTimestamp'])
    arrayRight = pd.DataFrame(df, columns=['Right', 'DeviceTimestamp', 'SystemTimestamp'])

    npleft = np.array(df[['DeviceTimestamp', 'Left']])
    npright = np.array(df[['DeviceTimestamp', 'Right']])

    for i in range(0, count_true(npleft)):
        current_line = npleft[i][1]['PupilDiameterValid']
        if current_line == 'false':
            npleft = np.delete(npleft, i, 0)

    for i in range(0, count_true(npright)):
        current_line = npright[i][1]['PupilDiameterValid']
        if current_line == 'false':
            npright = np.delete(npright, i, 0)
    return npleft, npright


class Prepare_for:

    def __init__(self, Array_db, array_new):
        self.Array_db = Array_db
        self.array_new = array_new

    # method to get and transform data from database or newdata
    # array_type : db or newdata
    # eye_side: left or right

    def get_data(self, array_type, eye_side):
        result = []
        array = self.array_new
        if array_type == 'db':
            array = self.Array_db

        for i in range(0, len(array)):
            df = pd.read_json(array[i])
            array_side = np.array(df[[eye_side, 'DeviceTimestamp']])
            result.append(array_side)

        return result

    # plot all  data from database or newdata
    # array_type : db or newdata
    # eye_side: left or right

    def plot_all_db(self, eye_side, array_type):
        _data = self.get_data(array_type, eye_side)
        plt.title("all in " + array_type + " for eye_" + eye_side)
        for j in range(0, len(_data)):
            current_plot = _data[j]
            x = [current_plot[i][1] for i in range(0, len(current_plot))]
            y = [current_plot[i][0]['PupilDiameter'] for i in range(0, len(current_plot))]
            plt.scatter(x, y, label="Person_" + str(j))
        plt.xlabel('timestamp')
        plt.ylabel('diameter')
        plt.legend(loc="upper left")
        plt.show()

    # method to prepare data for dtw and fast_dtw algorithm
    # array_type : db or newdata
    # eye_side: left or right

    def get_array_for_wrapping(self, array_type, eye_side):
        _data = self.get_data(array_type, eye_side)
        array_result = []
        for j in range(0, len(_data)):
            timestamp = [_data[j][i][1] for i in range(0, len(_data[j]))]
            diameter = [_data[j][i][0]['PupilDiameter'] for i in range(0, len(_data[j]))]

            array_result.append([timestamp, diameter])

        return array_result

import numpy as np
from Prepare_class import Prepare
from scipy.spatial.distance import euclidean
from dtw import *
import matplotlib.pyplot as plt
import seaborn


def get_Min(array):
    result = []
    mini = array[0]['distance']
    # print(type(mini))
    person_id = array[0]['person_id']
    for j in range(0, len(array)):
        if mini > float(array[j]['distance']):
            mini = array[j]['distance']
            person_id = array[j]['person_id']
    return mini, person_id


class Fast_dtw:
    data_session1 = []
    data_session2 = []
    array_threshold = []
    scene_id = 0

    def __init__(self, dataset, eye_side, scene_id, array_threshold):
        prepare = Prepare(dataset)
        self.data_session1, self.data_session2 = prepare.get_db_and_newdata(scene_id, eye_side)
        self.array_threshold = array_threshold
        self.scene_id = scene_id

    def run_fast_dtw(self):
        array_pp = []
        arr_confusion = []
        # 157283,
        array_tree = [214469, 364444]
        for tree in array_tree:
            tp, tn, fp, fn = 0, 0, 0, 0
            for d1 in self.data_session1:
                current_data = d1['left_right']
                id_person = current_data['identity']
                data_person = np.array(current_data['df_Left'][['System_time', 'add_operation']])
                # print(data_person)
                # print(current_data['df_Right'])

                # break
                distance_euclide = 0
                array_distance = []
                db_id_person = 0
                for d2 in self.data_session2:
                    db_data = d2['left_right']
                    db_id_person = db_data['identity']
                    db_person = np.array(db_data['df_Left'][['System_time', 'add_operation']])
                    print(db_person)
                    distance, cost_matrix, acc_cost_matrix, path = dtw(data_person, db_person, dist=euclidean)
                    print(distance)
                    array_distance.append({'distance': distance, 'person_id': db_id_person})
                mini, id = get_Min(array_distance)
                arr_confusion.append(id)

                print('for curent person' + str(id_person) + ' the authenticate person is person ' + str(
                    id) + 'with a '
                          'distance of ' + str(mini))

                if mini < tree and id == id_person:
                    tp += 1
                elif mini < tree and id != id_person:
                    fp += 1
                elif mini > tree and id not in [0, 1, 2, 3, 4, 5, 6, 7]:
                    fn += 1
                elif mini > tree and id in [0, 1, 2, 3, 4, 5, 6, 7]:
                    tn += 1
            array_pp.append(
                {'tp': tp, 'fp': fp, 'tn': tn, 'fn': fn, 'threshold': tree, 'Accuracy': (tp + tn) / (tp + fp + fn + tn),
                 'scene_id': self.scene_id})

        arr_threshold = [array_pp[i]['threshold'] for i in range(0, len(array_pp))]
        arr_x = [(array_pp[i]['fp'] / (array_pp[i]['fp'] + array_pp[i]['tn'])) for i in range(0, len(array_pp))]
        arr_y = [(array_pp[i]['fn'] / (array_pp[i]['fn'] + array_pp[i]['tp'])) for i in range(0, len(array_pp))]

        fig = plt.figure(figsize=(10, 10))
        plt.plot(arr_threshold, arr_x, label="FNR eye")
        plt.plot(arr_threshold, arr_y, label="FPR eye")
        plt.xlabel('threshold')
        plt.ylabel('Rate')
        plt.legend(loc="upper left")
        fig.savefig("dtw_scene_" + str(self.scene_id) + "FN and fpr.pdf")
        plt.show()

        labels = labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        data = []
        for i in range(0, 10):
            p = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            p[arr_confusion[i]] = 1
            data.append(p)
            # print(data)

        seaborn.set(color_codes=True)
        plt.figure(1, figsize=(10, 10))
        plt.title("dtw_confusion_matrix" + str(self.scene_id) + "_")
        seaborn.set(font_scale=1.4)
        ax = seaborn.heatmap(data, annot=True, cmap="Blues", cbar_kws={'label': 'Scale'})
        ax.set_xticklabels(labels)
        ax.set_yticklabels(labels)

        ax.set(ylabel="True Probanden", xlabel="Predicted Probanden")
        plt.savefig("dtw_confusion_matrix" + str(self.scene_id) + ".pdf", bbox_inches='tight', dpi=300)
        plt.show()
        # break
